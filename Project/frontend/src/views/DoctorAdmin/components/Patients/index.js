import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, mergeClasses } from '@material-ui/styles';
import {
  Grid
} from '@material-ui/core';
import Carousel from 'react-elastic-carousel'
import Item from "./Item";
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import Select from "react-select";

import "./styles.css";

// import { Swiper, SwiperSlide } from 'swiper/react';
// import 'swiper/swiper.scss';
// import 'swiper/swiper-bundle.min.css';
// import 'swiper/swiper.min.css';

const useStyles = makeStyles(() => ({
  root: {} ,
  time:{
    margin:'0',
    fontWeight:'bold',
    fontSize:'1rem'
  },
  day:{
    marginTop:'0.5rem',
    fontSize:'0.9rem'
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: 'white',
    // border: '2px solid #000',
    // boxShadow: theme.shadows[5],
    borderRadius:'0.5rem',
    padding: '1.6rem',
    fontFamily:"Roboto",
    outline:'none',
    // maxWidth: '30vw'
    width: '40%'
  },
  paperConfirmed: {
    backgroundColor: 'white',
    borderRadius:'0.5rem',
    padding: '3rem 2rem',
    fontFamily:"Roboto",
    outline:'none',
    width: '40vw',
    height:'50vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems:'center'
  },
  buttons:{
    display: 'flex',
    alignItems: 'center',
    marginTop:'1.5rem'
  },
  buttonsLR :{
    // padding: '0.3rem 3rem',
    fontSize:' 0.6rem',
    fontWeight: 600,
    margin: '0.5rem',
    backgroundColor: '#6E717D!important',
    borderRadius: '0.5rem'
  },
  textField:{
    width: '100%'
    
  }

}));

const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 550, itemsToShow: 2, itemsToScroll: 2 },
  { width: 768, itemsToShow: 4 },
  { width: 1200, itemsToShow: 4 }
];

const people = [
  "Общий анализ крови ",
  "СОЭ (Cкорость Оседания Эритроцитов)",
  "Резус-принадлежность",
  "Общий белок (в крови)",
  "Предрасположенность к тромбофилии - фолатный обмен (3 гена: MTR, MTRR, MTHFR)",
  "Ген метилентетрагидрофолатредуктазы - MTHFR (C677T)",
  "Гепатит А (anti-HAV IgG, anti-HAV IgM)"
];

const Patients = props => {
  // const { className } = props;
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [openConfirm, setOpenConfirm] = React.useState(false);
  const handleOpenConfirm = () => {
    setOpen(false);
    setOpenConfirm(true);
  };
  const handleCloseConfirm = () => {
    setOpenConfirm(false);
  };
  
  const [openComment, setOpenComment] = React.useState(false);
  const handleOpenComment = () => {
    setOpen(false);
    setOpenComment(true);
  };
  const handleCloseComment = () => {
    setOpenComment(false);
  };
  
  const [searchTerm, setSearchTerm] = React.useState("");
  // const [searchResults, setSearchResults] = React.useState([]);
  // const handleChange = e => {
  //   setSearchTerm(e.target.value);
  // };
  // React.useEffect(() => {
  //   const results = people.filter(person =>
  //     person.toLowerCase().includes(searchTerm)
  //   );
  //   setSearchResults(results);
  // }, [searchTerm]);

  const handleInputChange = (value, e) => {
    // if (e.action === "input-change") {
      setSearchTerm( value );
    // }
  };

  let options = [];

  options = options.concat(people.map(x => "P - " + x));

  function MakeOption(x) {
    return { value: x, label: x };
  }

  const marginTop = {
    backgroundColor:'#F6F6F6',
    height:'300px',
  }

  const items= [
    {id: 1, title: 'Пациент '+1+' хочет записаться',time:'Вторник 12:00'},
    {id: 2, title: 'Пациент '+2+' хочет записаться',time:'Вторник 16:00'},
    {id: 3, title: 'Пациент '+3+' хочет записаться',time:'ПОНЕДЕЛЬНИК 14:00'}
  ]
  return (
     
    // <Grid container style={marginTop}>
    //     <Grid container >
    <div style={marginTop}>
          <Carousel breakPoints={breakPoints} pagination={false} focusOnSelect={true}>
            {items.map((item) => (
                <Item key={item.id} onClick={handleOpen}>
                    <p className={classes.time}>{item.title}</p>
                    <p className={classes.day}>{item.time}</p> 
                    <Button variant="contained" color="primary" style={{padding: '0.5rem 3rem'}} onClick={handleClose}>Просмотреть</Button>
                </Item>
            ))}
          </Carousel>

          {/* Patient card opens */}
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper}>
                <h3 style={{color:'#1963C6'}} id="transition-modal-title">Султан Аиткали</h3>
                <Grid container justify="center" spacing={1} lg={10} >
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description">Имя:</p> 
                    </Grid> 
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description"><b>Султан</b></p> 
                    </Grid> 

                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description">Фамилия:</p> 
                    </Grid> 
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description"><b>Айткали</b></p> 
                    </Grid> 

                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description">Возраст:</p> 
                    </Grid> 
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description"><b>20</b></p> 
                    </Grid> 

                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description">Пол:</p> 
                    </Grid> 
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description"><b>М</b></p> 
                    </Grid> 

                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description">Симптоматика:</p> 
                    </Grid> 
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description"><b>Головная боль<br></br> Тошнота<br></br> Слабость</b></p> 
                    </Grid>
                </Grid>

                <div className={classes.buttons}>
                  <Button variant="contained" color="primary" className = {classes.buttonsLR} onClick={handleOpenComment}>комментарий</Button>
                  <Button variant="contained" color="primary" className = {classes.centerBtn} onClick={handleOpenConfirm}>принять</Button>
                  <Button variant="contained" color="primary" className = {classes.buttonsLR} onClick={handleClose}>перенаправить</Button>
                </div>
                
              </div>
            </Fade>
          </Modal>

          {/* Patient confirmed */}
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={openConfirm}
            onClose={handleOpenConfirm}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={openConfirm}>
              <div className={classes.paperConfirmed}>
                <h2 style={{color:'#1963C6',textAlign: 'center'}} id="transition-modal-title">Султан Аиткали успешно записан!</h2>
                <img src ={process.env.PUBLIC_URL + '/images/confirmed.png'} style={{width: '13vw'}}></img>

                <div className={classes.buttons} style={{justifyContent: 'flex-end'}}>
                    <Button variant="contained" style={{backgroundColor:'#847e7e',color:'white',textWeight:'600!important'}} onClick={handleCloseConfirm}>готово</Button>
                </div>
                
              </div>
            </Fade>
          </Modal>

          {/* Comment */}
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={openComment}
            onClose={handleCloseComment}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={openComment}>
              <div className={classes.paper}>
                <h3 style={{color:'#1963C6'}} id="transition-modal-title">Султан Аиткали</h3>
                <Grid container justify="center" spacing={1} lg={10} >
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description">Имя:</p> 
                    </Grid> 
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description"><b>Султан</b></p> 
                    </Grid> 

                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description">Фамилия:</p> 
                    </Grid> 
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description"><b>Айткали</b></p> 
                    </Grid> 

                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description">Возраст:</p> 
                    </Grid> 
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description"><b>20</b></p> 
                    </Grid> 

                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description">Пол:</p> 
                    </Grid> 
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description"><b>М</b></p> 
                    </Grid> 

                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description">Симптоматика:</p> 
                    </Grid> 
                    <Grid container item lg={6} xs={6} justify='right'>
                        <p id="transition-modal-description"><b>Головная боль<br></br> Тошнота<br></br> Слабость</b></p> 
                    </Grid>
                </Grid>
                <p>Рекомендуемые анализы: </p>
                <Select
                  isMulti
                  name="colors"
                  options={options.map(x => MakeOption(x))}
                  className="basic-multi-select"
                  classNamePrefix=""
                  closeMenuOnSelect={false}
                  onInputChange={handleInputChange}
                  inputValue={searchTerm}
                />
                {/* <div className="App">
                  <TextField
                    // label="Поиск"
                    id="filled-start-adornment"
                    className={classes.textField}
                    InputProps={{
                      startAdornment: <InputAdornment position="start"><SearchIcon></SearchIcon></InputAdornment>,
                    }}
                    variant="filled"
                    value={searchTerm}
                    onChange={handleChange}
                  />
                  <ul>
                    {searchResults.map(item => (
                      <li>{item}</li>
                    ))}
                  </ul>
                </div> */}

                <div className={classes.buttons}>
                  <Button variant="contained" color="primary" className = 'centerBtn' onClick={handleCloseComment}>сохранить</Button>
                </div>
                
              </div>
            </Fade>
          </Modal>

    </div>
        //  {/* </Grid> 
    // </Grid> */}
  );
};

Patients.propTypes = {
  className: PropTypes.string
};

export default Patients;
